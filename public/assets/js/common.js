/* eslint-disable no-unused-vars */
function getAjax(json, func) {
    $.ajax({
        type: "GET",
        url: "data/" + json,
        success: func,
        error: function(xhr, text, err) {
            // eslint-disable-next-line no-console
            console.error(xhr.status + " " + text + " " + err + " : " + json);
            ajaxError();
        }
    });
}

function ajaxError() {
    $("#main").html(
        '<p class="text-center text-info  py-5 w-100 ">Došlo je do greške, pokušajte kasnije</p>'
    );
}

function getAjaxMenu() {
    getAjax("menu.json", function(menu) {
        printTopMenu(menu);
    });
}

function printTopMenu(menu) {
    let html = "";
    menu.forEach(m => {
        html += printMenuItem(m);
    });
    $("#navHeader ul, #navFooter ul").html(html);
}

function printMenuItem(menu) {
    return `                        
    <li class="nav-item ${isActiveLink(menu)}">
        <a class="nav-link font-weight-bold" href="${menu.href}">${
        menu.text
    }</a>
    </li>
`;
}

function isActiveLink(menu) {
    return document.URL.indexOf(menu.href) != -1 ||
        (document.URL.indexOf(".html") == -1 && menu.href == "index.html")
        ? "active"
        : "";
}

function eventScroll() {
    if ($("html, body").scrollTop() > 180) {
        $("#toTop").css("display", "block");
    } else {
        $("#toTop").css("display", "none");
    }
}

function toTop() {
    $("html, body").animate(
        {
            scrollTop: 0
        },
        400
    );
}

function getLS(name) {
    return JSON.parse(localStorage.getItem(name));
}

function setLS(name, value) {
    return localStorage.setItem(name, JSON.stringify(value));
}

function removeLS(name) {
    return localStorage.removeItem(name);
}

function hasLS(name) {
    return localStorage.getItem(name) != null;
}

function printCartCount() {
    if (hasLS("books")) {
        let count = 0,
            books = getLS("books");
        for (let book of books) {
            count += book.quantity;
        }
        $("#cart-count").text(count);
    } else {
        $("#cart-count").text(0);
    }
}

$(window).on("scroll", eventScroll);

$("#toTop").on("click", toTop);

getAjaxMenu();

printCartCount();
