/* eslint-disable no-unused-vars */

$(document).ready(function() {
    $("form input:text, form input:password").on("blur", eventBlurField);
    $("form").on("reset", eventResetForm);
    $("form").on("submit", eventSubmitForm);
});

let valid, regExps;
regExps = {
    regMail: /^\w+([.-]?[\w\d]+)*@\w+([.-]?[\w]+)*(\.\w{2,4})+$/,
    regPass: /^(?=.*\d)(?=.*[A-zČĆŽŠĐčćžšđ])(?=.*[~!@#$%^&*<>?]).{6,}$/,
    regName: /^[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14}(\s[A-ZČĆŽŠĐ][a-zčćžšđ]{1,14})+$/
};

function eventBlurField() {
    let field = $(this);
    if (field.val().trim() === "") {
        field.removeClass("is-valid is-invalid");
    } else {
        singleFieldCheck(field, findRegExOfField(field));
    }
}

function eventResetForm() {
    $(this)
        .find("input:text, input:password")
        .removeClass("is-valid is-invalid");
}

function eventSubmitForm(event) {
    event.preventDefault();
    valid = true;
    let fields = $(this).find("input:text, input:password");
    for (let field of fields) {
        singleFieldCheck(field, findRegExOfField(field));
    }
    if (valid) {
        removeLS("books");
        printCartCount();
        alert("Uspeh");
    }
}

function singleFieldCheck(field, regularExp) {
    if (
        !regExps[regularExp].test(
            $(field)
                .val()
                .trim()
        )
    ) {
        valid = false;
        $(field).addClass("is-invalid");
    } else {
        $(field)
            .removeClass("is-invalid")
            .addClass("is-valid");
    }
}

function findRegExOfField(field) {
    return (
        "reg" +
        $(field)
            .prop("name")
            .substring(2)
    );
}
