/* eslint-disable no-undef */
$(document).ready(function() {
    AOS.init({
        duration: 700,
        easing: "ease-in-out",
        once: true
    });
    getAjaxBooks();
    getAjaxGenres();
    $("#resetFilters").on("click", eventResetFilters);
    $(".sort").on("click", eventSort);
    $("#search").on("keyup", eventSearch);
});

//#region Global variables

let ajaxBooks = [],
    genreBooks = [],
    ajaxGenres = [],
    currentFilterPublisher = [],
    currentFilterPriceRange = [],
    currentSort = null;

//#endregion Global variables

//#region Object Constructors

function BookObj(
    id,
    name,
    description,
    picture,
    price,
    discount,
    year,
    pages,
    publisher,
    authors,
    genres
) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.picture = picture;
    this.price = price;
    this.discount = discount;
    this.year = year;
    this.pages = pages;
    this.publisher = publisher;
    this.authors = authors;
    this.genres = genres;

    this.printBook = () => {
        return `
            <div class="book mb-2 col-md-4 col-sm-6" data-aos="fade-up">
                <div class="card border mx-md-0 mx-lg-1">
                    <img class="card-img-top lazyload" data-src="assets/images/${
                        this.picture.src
                    }" alt="${this.picture.alt}">
                    <div class="card-body pt-0 text-center">
                    ${this.printDicountPercentage()}
                        <h5 class="card-title text-primary text-uppercase">${
                            this.name
                        }</h5>
                        <h6 class="card-subtitle text-success">${this.printAuthorsForBook()}</h6>
                        <p class="my-1">${this.printPrice()}</p>
                        <button type="button" class="btn btn-outline-primary details" data-toggle="modal" data-target="#bookDetails" data-id="${
                            this.id
                        }">Detalji</button>
                        <a href="#" data-id="${
                            this.id
                        }" class="buy mx-1 btn btn-outline-success">Kupi</a>
                    </div>
                </div>
            </div>
        `;
    };

    this.printBookDetails = () => {
        return `
            <div class="container">
                <div class="row">
                    <div class="col-md-7 mb-0">
                        <h5 class="text-uppercase text-primary">${
                            this.name
                        }</h5>
                        <h6>${this.printGenresForBook()}</h6>
                        <h6 class="text-success">${this.printAuthorsForBook()}</h6>
                        <p class="border-bottom mb-0 ">${this.description}</p>
                        <div class="p-1 d-flex justify-content-between align-items-center">
                            <div>${this.printBookDetailsPrice()}</div>
                            <div>
                            <a href="#" data-id="${
                                this.id
                            }" class="buy-details mx-1 btn btn-outline-success">Kupi</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 order-md-first mt-0">
                        <div>
                            <ul class="list-group list-group-flush mb-2">
                                <li class="list-group-item p-1">Izdavač: ${
                                    this.publisher.name
                                }</li>
                                <li class="list-group-item p-1">Broj strana: ${
                                    this.pages
                                }</li>
                                <li class="list-group-item p-1">Godina izdanja: ${
                                    this.year
                                }</li>
                            </ul>
                        </div>
                        <img class="img-fluid lazyload" data-src="assets/images/${
                            this.picture.src
                        }" alt="${this.picture.alt}">
                        ${this.printDicountPercentage()}
                    </div>
                </div>
            </div>
        `;
    };

    this.calcDiscountPrice = () =>
        Math.round(this.price * (1 - this.discount / 100));

    this.returnCalculatedPrice = () =>
        this.discount ? this.calcDiscountPrice() : this.price;

    this.printDicountPercentage = () => {
        return this.discount
            ? `<span class="badge badge-danger w-100">${
                  this.discount
              }% popusta</span>`
            : "";
    };

    this.printPrice = () => {
        let html;
        html = `<span class="text-danger font-weight-bold">${this.returnCalculatedPrice()} RSD</span>`;
        if (this.discount) {
            html += `<del class="m-1 text-muted small">${this.price} RSD</del>`;
        }
        return html;
    };

    this.printBookDetailsPrice = () => {
        if (this.discount) {
            return `
                <p class="my-0"><del class="text-muted small">${
                    this.price
                } RSD</del></p>
                <p class="text-danger my-0 font-weight-bold">${this.returnCalculatedPrice()} RSD</p>
                <p class="font-weight-bold text-primary small my-0">Ušteda ${this
                    .price - this.returnCalculatedPrice()} RSD</p>
            `;
        } else {
            return `
                <p class="text-danger my-0 font-weight-bold">$${this.price}</p>
            `;
        }
    };

    this.printAuthorsForBook = () => {
        let html = "";
        this.authors.forEach((author, index) => {
            index == 0 ? (html += author) : (html += ", " + author);
        });
        return html;
    };

    this.printGenresForBook = () => {
        let html = "";
        this.genres.forEach((genre, index) => {
            index == 0 ? (html += genre.name) : (html += ", " + genre.name);
        });
        return html;
    };
}

function PublisherObj(id, name) {
    this.id = id;
    this.name = name;

    this.printPublisher = books => {
        return `
            <div class="form-check">
                <input class="form-check-input  publisher" type="checkbox" value="${
                    this.id
                }" id="publisher-${this.id}" />
                <label class="form-check-label d-flex" for="publisher-${
                    this.id
                }">
                    ${
                        this.name
                    }   <span class="ml-auto">(${printNumOfBooksByCriterion(
            books,
            this.id
        )})</span>
                </label>
            </div>
        `;
    };
}

function GenreObj(id, name) {
    this.id = id;
    this.name = name;

    this.printGenre = () => {
        return `
            <li class="list-unstyled"><a href="#" data-id="${
                this.id
            }" class="genre">${this.name.toUpperCase()}</a></li>
        `;
    };
}

//#endregion Object Constructors

//#region AJAX

function getAjaxBooks() {
    getAjax("books.json", function(books) {
        for (let book of books) {
            ajaxBooks.push(
                new BookObj(
                    book.id,
                    book.name,
                    book.description,
                    book.picture,
                    book.price,
                    book.discount,
                    book.year,
                    book.pages,
                    book.publisher,
                    book.authors,
                    book.genres
                )
            );
        }
        genreBooks = [...ajaxBooks];
        showAjaxBooks();
        if (localStorage.length) {
            showLSBooks();
        }
    });
}

function getAjaxGenres() {
    getAjax("genres.json", function(genres) {
        for (let genre of genres) {
            ajaxGenres.push(new GenreObj(genre.id, genre.name));
        }
        printGenres(ajaxGenres);
        if (hasLS("genre")) {
            printGenreName(getLS("genre"));
        }
    });
}

//#endregion AJAX

//#region Show

function showBooks(books) {
    printBooks(books);
    showPublishers(books);
    showPriceRanges(books);
}

function showAjaxBooks() {
    showBooks(ajaxBooks);
}

function showGenreBooks() {
    showBooks(genreBooks);
}

function showLSBooks() {
    let publishers,
        priceRanges,
        books = [...ajaxBooks];
    if (hasLS("sort")) {
        currentSort = getLS("sort");
    }
    if (hasLS("genre")) {
        books = filterByGenre(getLS("genre"));
        showGenreBooks();
    }
    if (hasLS("publishers")) {
        publishers = getLS("publishers");
        books = filterByPublisher(genreBooks, publishers);
        currentFilterPublisher = publishers;
    }
    if (hasLS("priceRanges")) {
        priceRanges = getLS("priceRanges");
        books = filterByPriceRange(books, priceRanges);
        currentFilterPriceRange = priceRanges;
    }
    if (books.length) {
        printBooks(books);
        showPublishers(genreBooks);
        showPriceRanges(genreBooks);
        if (priceRanges) {
            $(".price-range").each(function() {
                for (let priceRange of priceRanges) {
                    if ($(this).val() == priceRange) {
                        $(this).prop("checked", true);
                    }
                }
            });
        }
        if (publishers) {
            $(".publisher").each(function() {
                for (let publisher of publishers) {
                    if ($(this).val() == publisher) {
                        $(this).prop("checked", true);
                    }
                }
            });
        }
    }
}

function showFilteredBooks() {
    let books = genreBooks;
    if (currentFilterPublisher.length && currentFilterPriceRange.length) {
        books = filterByPublisher(books, currentFilterPublisher);
        books = filterByPriceRange(books, currentFilterPriceRange);
    } else if (currentFilterPublisher.length) {
        books = filterByPublisher(books, currentFilterPublisher);
        showPriceRanges(books);
    } else if (currentFilterPriceRange.length) {
        books = filterByPriceRange(books, currentFilterPriceRange);
        showPublishers(books);
    } else {
        showPublishers(books);
        showPriceRanges(books);
    }
    printBooks(books);
}

function getCurrentBooks() {
    let currentBooksIds = [];
    $(".book").each(function() {
        currentBooksIds.push(
            $(this)
                .find("button.details")
                .data("id")
        );
    });
    return ajaxBooks.filter(ajaxBook =>
        currentBooksIds.some(currentBookId => currentBookId == ajaxBook.id)
    );
}

function showPublishers(books) {
    let differentPublishers = [];
    for (let book of books) {
        if (isUniquePublisher(differentPublishers, book.publisher)) {
            differentPublishers.push(
                new PublisherObj(book.publisher.id, book.publisher.name)
            );
        }
    }
    printPublishers(differentPublishers, books);
}

function showPriceRanges(books) {
    let prices = [],
        rangesValues = [],
        ranges = [1];
    let lowMid, highMid, median, step, stepTo;

    books.forEach(book => {
        prices.push(book.returnCalculatedPrice());
    });
    prices.sort((a, b) => a - b);
    lowMid = Math.floor((prices.length - 1) / 2);
    highMid = Math.ceil((prices.length - 1) / 2);
    median = (prices[lowMid] + prices[highMid]) / 2;
    step = Math.floor(median / 4 / 50) * 50;
    for (let price of prices) {
        stepTo = Math.ceil(price / step) * step + 1;
        if (ranges.indexOf(stepTo) === -1) {
            ranges.push(stepTo);
        }
    }
    for (let i = 0; i < ranges.length - 1; i++) {
        rangesValues.push(ranges[i] + "-" + ranges[i + 1]);
    }
    printPriceRanges(rangesValues, books);
}

function isUniquePublisher(differentPublishers, publisher) {
    let isUnique = false;
    if (differentPublishers.length > 0) {
        let differentPublishersIds = differentPublishers.map(
            diffPublisher => diffPublisher.id
        );
        if (!inArray(differentPublishersIds, publisher.id)) {
            isUnique = true;
        }
    } else {
        isUnique = true;
    }
    return isUnique;
}

function inArray(array, element) {
    return array.indexOf(element) !== -1;
}

//#endregion Show

//#region Print

function printBooks(books) {
    if (!books.length) {
        printNoBooks();
        return;
    }
    let html = "";
    if (currentSort) {
        sortBooks(books, currentSort.by, currentSort.order);
    }
    books.forEach(book => {
        html += book.printBook();
    });
    $("#books").html(html);
    $("#bookDetails").on("show.bs.modal", eventDetails);
    $(".buy").on("click", eventBuy);
}

function printGenres(genres) {
    let html =
        '<li class="list-unstyled"><a href="#" data-id="0" class="genre">SVI ŽANROVI</a></li>';
    genres.forEach(genre => {
        html += genre.printGenre();
    });
    $("#genres").html(html);
    $(".genre").on("click", eventFilterByGenre);
}

function printPublishers(publishers, books) {
    let html = "";
    publishers.forEach(publisher => {
        html += publisher.printPublisher(books);
    });
    $("#publishers").html(html);
    $(".publisher").on("change", eventFilterByPublisher);
}

function printPriceRanges(priceRanges, books) {
    let html = "";
    priceRanges.forEach(priceRange => {
        html += printPriceRange(priceRange, books);
    });
    $("#price-ranges").html(html);
    $(".price-range").on("change", eventFilterByPriceRange);
}

function printPriceRange(priceRange, books) {
    return `
        <div class="form-check">
            <input class="form-check-input price-range" type="checkbox" value="${priceRange}" id="price-range-${priceRange}" />
            <label class="form-check-label d-flex" for="price-range-${priceRange}">
                ${priceRange} <span class="ml-auto">(${printNumOfBooksByCriterion(
        books,
        priceRange
    )})</span>
            </label>
        </div>
    `;
}

function printNoBooks() {
    $("#books").html(
        '<p class="text-center text-info  py-5 w-100">Ne postoje knjige koje ispunjavaju uslov(e)<p>'
    );
}

function printNumOfBooksByCriterion(books, criteria) {
    let callback =
        typeof criteria == "string"
            ? filterByPriceRangeSingleBook
            : filterByPublisherSingleBook;
    return filterByCriterion(books, criteria, callback).length;
}

function printGenreName(id) {
    if (id == 0) {
        $("#genre-name").html("SVI ŽANROVI");
    } else {
        let genre = ajaxGenres.find(genre => genre.id == id);
        $("#genre-name").html(genre.name.toUpperCase());
    }
}

//#endregion Print

//#region Event

function eventDetails(e) {
    let bookId, book;
    bookId = $(e.relatedTarget).data("id");
    book = ajaxBooks.find(book => book.id == bookId);
    $(this)
        .find(".modal-body")
        .html(book.printBookDetails());
    $(".buy-details").on("click", eventBuyDetails);
}

function eventBuy(e) {
    e.preventDefault();
    let id = $(this).data("id");
    addToCart(id);
    toTop();
}

function eventBuyDetails(e) {
    e.preventDefault();
    let id = $(this).data("id");
    $("#bookDetails").modal("hide");
    addToCart(id);
    toTop();
}

function eventSearch() {
    let input = this.value;
    let books = genreBooks.filter(book => {
        if (book.authors.some(author => ifItMatches(author))) {
            return true;
        }
        return ifItMatches(book.name) || ifItMatches(book.description);
    });
    function ifItMatches(what) {
        if (what.toLowerCase().indexOf(input.toLowerCase()) !== -1) {
            return true;
        }
    }
    printBooks(books);
}

function eventResetFilters() {
    currentFilterPublisher.length = 0;
    currentFilterPriceRange.length = 0;
    genreBooks = [...ajaxBooks];
    printGenreName(0);
    currentSort = null;
    removeLS("genre");
    removeLS("publishers");
    removeLS("priceRanges");
    showAjaxBooks();
}

function eventSort(e) {
    e.preventDefault();
    let by, order, books;
    by = $(this).data("by");
    order = $(this).data("order");
    currentSort = { by: by, order: order };
    setLS("sort", currentSort);
    books = getCurrentBooks();
    sortBooks(books, by, order);
    printBooks(books);
}

function sortBooks(books, by, order) {
    books.sort((a, b) => {
        let valueA = by == "price" ? a.returnCalculatedPrice() : a.name;
        let valueB = by == "price" ? b.returnCalculatedPrice() : b.name;
        if (valueA > valueB) return order == "asc" ? 1 : -1;
        else if (valueA < valueB) return order == "asc" ? -1 : 1;
        else return 0;
    });
}

function eventFilterByGenre(e) {
    e.preventDefault();
    let id = $(this).data("id");
    if (!id) {
        genreBooks = [...ajaxBooks];
        showGenreBooks();
        removeLS("genre");
    } else {
        filterByGenre(id);
        showGenreBooks();
        setLS("genre", id);
    }
    printGenreName(id);
}

function eventFilterByPublisher() {
    let checkedPublishers = $(".publisher:checked");
    if (!checkedPublishers.length) {
        currentFilterPublisher.length = 0;
        showFilteredBooks();
        removeLS("publishers");
    } else {
        let checkedValues = [];
        for (let c of checkedPublishers) {
            checkedValues.push(c.value);
        }
        currentFilterPublisher = checkedValues;
        showFilteredBooks();
        setLS("publishers", checkedValues);
    }
}

function eventFilterByPriceRange() {
    let checkedPriceRanges = $(".price-range:checked");
    if (!checkedPriceRanges.length) {
        currentFilterPriceRange.length = 0;
        showFilteredBooks();
        removeLS("priceRanges");
    } else {
        let checkedValues = [];
        for (let c of checkedPriceRanges) {
            checkedValues.push(c.value);
        }
        currentFilterPriceRange = checkedValues;
        showFilteredBooks();
        setLS("priceRanges", checkedValues);
    }
}

function addToCart(id) {
    let books = getLS("books");
    if (books) {
        if (bookIsAlreadyInCart()) {
            updateQuantity();
        } else {
            addBookToLocalStorage();
        }
    } else {
        addFirstBookToLocalStorage();
    }

    printCartCount();

    function bookIsAlreadyInCart() {
        return books.filter(book => book.id == id).length;
    }

    function updateQuantity() {
        let books = getLS("books");
        for (let book in books) {
            if (books[book].id == id) {
                books[book].quantity++;
                break;
            }
        }
        setLS("books", books);
    }

    function addBookToLocalStorage() {
        let books = getLS("books");
        books.push({
            id: id,
            quantity: 1
        });
        setLS("books", books);
    }

    function addFirstBookToLocalStorage() {
        let books = [];
        books[0] = {
            id: id,
            quantity: 1
        };
        setLS("books", books);
    }
}

//#endregion Event

//#region Filter

function filterByGenre(id) {
    genreBooks = ajaxBooks.filter(book =>
        book.genres.some(genre => genre.id == id)
    );
    return genreBooks;
}

function filterByPublisher(books, publisherIds) {
    return books.filter(book => {
        for (let publisherId of publisherIds) {
            if (filterByPublisherSingleBook(book, publisherId)) {
                return true;
            }
        }
    });
}

function filterByPriceRange(books, priceRanges) {
    return books.filter(book => {
        for (let priceRange of priceRanges) {
            if (filterByPriceRangeSingleBook(book, priceRange)) {
                return true;
            }
        }
    });
}

function filterByPublisherSingleBook(book, publisherId) {
    if (book.publisher.id == publisherId) {
        return true;
    }
    return false;
}

function filterByPriceRangeSingleBook(book, priceRange) {
    let priceFrom, priceTo, price;
    priceFrom = Number(priceRange.split("-")[0]);
    priceTo = Number(priceRange.split("-")[1]);
    price = book.returnCalculatedPrice();
    if (price >= priceFrom && price <= priceTo) {
        return true;
    }
}

function filterByCriterion(books, criteria, callback) {
    return books.filter(book => {
        return callback(book, criteria);
    });
}

//#endregion Filter
